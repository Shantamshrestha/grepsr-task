const PROTO_PATH = 'stream_csv.proto';
const async = require('async');
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const { exception } = require('console');
const { exit } = require('process');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
let proto = grpc.loadPackageDefinition(packageDefinition).csv;
let client = new proto.Stream('localhost:5000', grpc.credentials.createInsecure());

let fetchCSV = (callback) => {

    if (! process.env.URL) {
        console.log("Error: Need to give url");
        exit(1);
    }

    let url = process.env.URL;
    let csvData = { url: url }
    let call = client.csvToObject(csvData);

    call.on('data', (data) => {
        console.log(data.row);
    });
    call.on('end', () => {
        console.log("Finished")
    });
    call.on('error', (err) => {
        if (err.code == grpc.status.NOT_FOUND) {
            console.log("Error: Please check if the url is valid or not");
            console.log("Enter url without protocol:");
            console.log("Example: 2fc67fed9430.ngrok.io/server/500K.csv")
        }
        else if(err.code == grpc.status.UNKNOWN){
            console.log(err.details);
        }
        else if(err.code == grpc.status.UNAVAILABLE){
            console.log("Server unavailable")
        }
        // Keep adding exceptions here
    });
}

let main = () => {
    async.series([
        fetchCSV,
    ]);
}

if (require.main === module) {
    main();
}

exports.fetchCSV = fetchCSV;