import grpc
import stream_csv_pb2
import stream_csv_pb2_grpc
import urllib.request
from urllib.error import HTTPError, URLError
from concurrent import futures
import logging
from os import environ

class Stream(stream_csv_pb2_grpc.StreamServicer):

    def csvToObject(self, request, context):
        url = request.url
        protocol = stream_csv_pb2.CSVDataRequest.Protocol.Name(
            request.protocol)
        fetch_url = protocol.lower() + "://"+url

        try:
            with urllib.request.urlopen(fetch_url) as data:
                for line in data:
                    decoded_line = line.decode()
                    val = decoded_line.split(',')
                    yield stream_csv_pb2.CSVDataResponse(row=val)

        except URLError as e:
            context.abort(grpc.StatusCode.NOT_FOUND,
                        'Error in URL.')

    def sayHello(self, request, context):
        name = request.name
        print(name)
        return stream_csv_pb2.HelloReply(message='Hello %s' % (name))




def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    stream_csv_pb2_grpc.add_StreamServicer_to_server(Stream(), server)
    server.add_insecure_port('[::]:%d' %(PORT))
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    try:
        PORT = int(environ.get('PORT',5000))
    except ValueError:
        PORT = 5000

    logging.basicConfig(level=logging.DEBUG)
    serve()
