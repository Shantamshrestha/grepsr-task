import grpc

import stream_csv_pb2
import stream_csv_pb2_grpc

def run():
    channel = grpc.insecure_channel('localhost:5000')
    stub = stream_csv_pb2_grpc.StreamStub(channel)

    response_list = stub.csvToObject(stream_csv_pb2.CSVDataRequest(url='2fc67fed9430.ngrok.io/server/500K.csv'))
    for response in response_list:
        print(response.row)


if __name__ == '__main__':
    run()