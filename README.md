# Instructions:

##  To start gRPC Python server:

```
cd ./server
docker build --tag csv_server .
docker run -it --network=host --name server csv_server
```
## To start gRPPC Node client:

```
cd ./client
docker build --tag csv_cleint . 
docker run -it --env URL='<URL>' --network=host csv_client  

# Enter url without protocol eg. locahost:5500/sample.csv

```
